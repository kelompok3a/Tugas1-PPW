from django.shortcuts import render
from .models import Data

# Create your views here.
response = {}

def index(request):
	data = Data.objects.all()[0]
	response['data'] = data
	
	expertise = data.expertise
	expertiseSplit = expertise.split(",")
	response['expertise'] = expertiseSplit
	
	html = 'app_2/app_2.html'
	return render(request, html, response)