from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index, response
from .models import Data

# Create your tests here.
class App2UnitTest(TestCase):

	def test_app2_url_is_exist(self):
		data = Data.objects.create(name="Rafif Iqbal Shaputra",birthday="1 January 1998",gender="Male",expertise="Marketing,Collector,Public Speaking",\
				description="Aesthetique Sekali",email="rafif.doang@gmail.com",imageurl="https://www.w3schools.com/howto/img_avatar.png")
		response = Client().get('/app-2/')
		self.assertEqual(response.status_code, 200)

	def test_app2_using_index_func(self):
		data = Data.objects.create(name="Rafif Iqbal Shaputra",birthday="1 January 1998",gender="Male",expertise="Marketing,Collector,Public Speaking",\
				description="Aesthetique Sekali",email="rafif.doang@gmail.com",imageurl="https://www.w3schools.com/howto/img_avatar.png")
		found = resolve('/app-2/')
		self.assertEqual(found.func, index)
		
	def test_landing_page_is_completed(self):
		nama = "Rafif Iqbal Shaputra"
		data = Data.objects.create(name=nama,birthday="1 January 1998",gender="Male",expertise="Marketing,Collector,Public Speaking",\
				description="Aesthetique Sekali",email="rafif.doang@gmail.com",imageurl="https://www.w3schools.com/howto/img_avatar.png")
	
		request = HttpRequest()
		response = index(request)
		html_response = response.content.decode('utf8')

		#Checking whether have Bio content
		self.assertIn(nama, html_response)
		
	def test_model_can_create_new_message(self):
		#Creating a new activity
		data = Data.objects.create(name="Rafif Iqbal Shaputra",birthday="1 January 1998",gender="Male",expertise="Marketing,Collector,Public Speaking",\
				description="Aesthetique Sekali",email="rafif.doang@gmail.com",imageurl="https://www.w3schools.com/howto/img_avatar.png")

		#Retrieving all available activity
		counting_all_available_data = Data.objects.all().count()
		self.assertEqual(counting_all_available_data,1)
		
	def test_lab_4_str_in_models(self):
		nama = 'Rafif Iqbal Shaputra'
		data = Data.objects.create(name=nama,birthday="1 January 1998",gender="Male",expertise="Marketing,Collector,Public Speaking",\
				description="Aesthetique Sekali",email="rafif.doang@gmail.com",imageurl="https://www.w3schools.com/howto/img_avatar.png")
		self.assertEqual(str(data), nama)