from django.db import models

# Create your models here.
class Data(models.Model):
	name = models.CharField(max_length=25)
	birthday = models.CharField(max_length=20)
	gender = models.CharField(max_length=6)
	expertise = models.TextField()
	description = models.TextField()
	email = models.EmailField()
	imageurl = models.TextField()
	
	def __str__(self):
		return self.name