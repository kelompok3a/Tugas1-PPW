from django.shortcuts import render
from django.http import HttpResponseRedirect
from app_1.models import Status
from app_3.models import Friend

response = {}
def index(request):
    author = 'Rafif Iqbal Shaputra'
    response['author'] = author

    friends = len(Friend.objects.all())
    response['friends'] = friends

    feeds = len(Status.objects.all())
    response['feeds'] = feeds

    last = Status.objects.last()
    response['last'] = last


    html = 'app_4.html'
    return render(request, html, response)
