from django.test import TestCase

import sys
sys.path.append(sys.path[0] + '/..')

from django.test import TestCase
from django.test import Client
from django.utils import timezone

from unittest import skip

from app_1.models import Status
from app_3.models import Friend
from app_2.models import Data
from datetime import date

# Create your tests here.
class UnitTest(TestCase):
 def setUp(self):
  profile = Data.objects.create(name="Guest", birthday=date(1900, 1, 1))

 def test_stats_is_exist(self):
  response = Client().get('/app-4/')
  self.assertEqual(response.status_code, 200)

 def test_stats_has_status_count(self):
  status_dummy = Status.objects.create(description='dummy status')

  response = Client().get('/app-4/')
  html_response = response.content.decode('utf8')

  self.assertIn('1 feeds', html_response)

 def test_stats_has_friend_count(self):
  friend_dummy = Friend.objects.create(name='dummy friend', url='http://dummyurl.friend/')

  response = Client().get('/app-4/')
  html_response = response.content.decode('utf8')

  self.assertIn('1 people', html_response)

 def test_stats_has_latest_post(self):
  status_dummy = Status.objects.create(description='dummy status')

  response = Client().get('/app-4/')
  html_response = response.content.decode('utf8')

  self.assertIn(status_dummy.description, html_response)


  response = Client().get('/app-4/')
  html_response = response.content.decode('utf8')

  self.assertIn('dummy status', html_response)
