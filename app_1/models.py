from django.db import models

class Status(models.Model):
	description = models.CharField(max_length=140)
	created_date = models.DateTimeField(auto_now_add=True)
