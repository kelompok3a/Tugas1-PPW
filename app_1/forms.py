from django import forms

class Status_Form(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
    }
   
    
    description_attrs = {
        'type': 'text',
				
        'class': 'status-form-textarea',
        'placeholder':'Apa yang anda pikirkan?...'
    }

    
    description = forms.CharField(label='', required=True, widget=forms.Textarea(attrs=description_attrs))
	