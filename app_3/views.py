from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Friend_Form
from .models import Friend

response = {'author' : "Kelompok 3A"}

def index(request):
    response['content'] = 'sesuatu'
    html = 'app_3/app_3.html'
    friend = Friend.objects.all()
    friend = reversed(friend)
    response['friend'] = friend
    response['friend_form'] = Friend_Form
    print(response['friend_form'])
    return render(request, html, response)

def add_friend(request):
    form = Friend_Form(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        response['name'] = request.POST['name']
        response['url'] = request.POST['url']
        friend = Friend(name=response['name'], url=response['url'])
        friend.save()
        return HttpResponseRedirect('/app-3/')
    else:
        print("tidak valid")
        return HttpResponseRedirect('/app-3/')
