from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .views import index, add_friend
from .models import Friend
from .forms import Friend_Form

# Create your tests here.
class Lab5UnitTest(TestCase):

    def test_app_3_url_is_exist(self):
        response = Client().get('/app-3/')
        self.assertEqual(response.status_code, 200)

    def test_app_3_using_index_func(self):
        found = resolve('/app-3/')
        self.assertEqual(found.func, index)

    def test_model_can_add_friend(self):
        # Creating a new friend
        new_friend = Friend.objects.create(name='nathan', url='http://nathan.herokuapp.com')

        # Retrieving all available activity
        counting_all_friend = Friend.objects.all().count()
        self.assertEqual(counting_all_friend, 1)
  
    def test_form_validation_for_blank_items(self):
        form = Friend_Form(data={'name': '', 'url': ''})
        self.assertFalse(form.is_valid())
        self.assertEqual(
            form.errors['name'],
            ["This field is required."]
        )

    def test_app3_post_success_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/app-3/add_friend', {'name': test, 'url': 'http://hehe.herokuapp.com'})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/app-3/')
        html_response = response.content.decode('utf8')
        self.assertIn(test, html_response)

    def test_app3_post_with_invalid_regex_and_render_the_result(self):
        test = 'Anonymous'
        response_post = Client().post('/app-3/add_friend', {'name': test, 'url': ''})
        self.assertEqual(response_post.status_code, 302)

        response= Client().get('/lab-5/')
        html_response = response.content.decode('utf8')
        self.assertNotIn(test, html_response)

    
